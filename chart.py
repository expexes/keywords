#!/usr/bin/python3

import matplotlib
import matplotlib.pyplot as plt
import numpy as np

import utils
import logic





FILE_NAME = 'text.txt'

TEXT_PATH = 'data/' + FILE_NAME
IGNORED_WORDS_PATH = 'data/ignored_words.txt'

IGNORED_WORDS = set(open(IGNORED_WORDS_PATH, 'r').read().split(','))
IGNORED_SYMBOLS = set([
	',', '.', ';', ':', ' - '
])

text = open(TEXT_PATH, 'r').read()

words = logic.prepare(text, IGNORED_WORDS, IGNORED_SYMBOLS)

freq = logic.calculate_frequency(words)

rank = logic.calculate_rang_by_frequency(freq)


rank_min = min(rank.values())
rank_max = max(rank.values())

# print(freq)
# print()
# print(dict(sorted(rank.items(), key=lambda kv: kv[1])))


# t = np.arange(rank_min, rank_max, 1)
# s = f()

fig, ax = plt.subplots()
ax.plot(dict(sorted(rank.items(), key=lambda kv: kv[1])).values(), freq.values())

plt.show()
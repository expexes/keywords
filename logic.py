import utils
import operator
from collections import Counter

# preparing a text
# - text to array
# - words to lowercase
# - deleting useless symbols & words
# return array of words
def prepare(text, ignored_words = [], ignored_symbols = []):
	for ignored_symbol in ignored_symbols:
		text = text.replace(ignored_symbol, '')
		
	words = [word.lower() for word in text.split()]

	for ignored_word in ignored_words:
		if ignored_word in words:
			utils.remove_all(words, ignored_word)

	return words


def calculate_frequency(words):
	return Counter(words)


# https://stackoverflow.com/a/30282676
def calculate_rang_by_frequency(words_frequency):
	r = {key: rank for rank, key in enumerate(sorted(set(words_frequency.values()), reverse=True), 1)}
	return {k: r[v] for k,v in words_frequency.items()}
